# Installer un serveur Plaanet

Ce fichier décrit l'ensemble des manipulations à effectuer pour installer l'API Plaanet sur votre propre serveur de production (accessible à tous via le web), et pour ajouter votre serveur à la liste des serveurs du réseau.

Vous devrez peut-être ajuster cette procédure en fonction des caractéristiques de votre serveur, et des erreurs qui pourront survenir pendant l'installation.

Les indications données ici ont été vérifiées pour Ubuntu-20, à vous d'adapter le/les scripts en fonction de votre configuration si besoin.

---

### Comment configurer les paramètres de mon API (nom de domaine, port, etc) ? 

---

Toute l'installation se fait automatiquement grace à un script à executer sur votre serveur.

Ce script vous demandera d'indiquer toutes les valeurs dont il a besoin dès son lancement. 

Tapez sur Entrée à chaque étape pour utiliser les valeurs par défaut (sauf pour le nom de domaine). Ou indiquez d'autres valeurs si vous avez besoin d'une configuration différente (si vous hébergez plusieurs API sur un même serveur, vous avez peut-être besoin d'utiliser d'autres ports par exemple) 

Valeurs configurables : 

- instance_domaine : votre nom de domaine
- instance_port : port d'entrée principal du reverse-proxy, pour accéder à l'api via le nom de domaine (default : 444) (ex: https://plaanet.io:444)
- instance_port_socket : port d'entrée du reverse-proxy, utilisé pour les sockets (default : 445)
- bind_port_https : port sur lequel se lance l'API en local, derrière le reverse-proxy (default: 3001)
- bind_port_socket : port socket local, derrière le reverse-proxy (default: 3002)
- root_node_url : url du noeud de référence du réseau (default : https://plaanet.io)
- root_node_port : du noeud de référence du réseau (default : 444)
- db_name : nom de la base de donnée (default : plaanet)
- chain_name : nom de la multichain (blockchain)


---

### De quoi a-t-on besoin pour faire fonctionner l'API Plaanet ? 

---

NodeJS
- https://github.com/nodesource/distributions/blob/master/README.md

MongoDB (base de données) 
- https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

Multichain (blockchain permettant de sécuriser les votes des sondages et référendum) 
- https://www.multichain.com/download-community/

Caddy (reverse-proxy auto ssl) 
- https://caddyserver.com/docs/api

---

### Prérequis : 

- Vous devez disposer d'un serveur accessible en ligne, avec un nom de domaine valide.

- Par défaut, les ports utilisés par l'API en local sont le 3001 et 3002 (vous pouvez choisir d'autres ports si besoin, pendant la procédure d'installation).

- L'API ne gère pas le protocole SSL elle-même.
Pour accéder à l'API en httpS, il est nécessaire de mettre en place un reverse-proxy (Caddy), pour rediriger les requêtes vers les ports de l'API (3001 et 3002 par défaut). Ce reverse-proxy est installé et configuré automatiquement par le script d'installation.

- Les ports par défaut du reverse-proxy sont le 444 qui redirige vers 3001, et 445 qui redirige vers 3002. 

444>3001 et 445>3002

> Pour pouvoir accéder à l'API, il est donc nécessaire d'ouvrir les ports 444 et 445 manuellement sur votre serveur (configuration du firewall). En général, cette opération s'effectue via l'interface de votre hébergeur (OVH, 1&1, Infomaniak, etc)

---

## Installer Git et NodeJS : 
Si cela n'est pas déjà fait, vous devez commencer par installer NodeJS et Git sur votre serveur, afin de pouvoir télécharger et installer l'application : 

### Git

https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git

`sudo apt install git-all`

### NodeJS

https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

`curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -`

`sudo apt-get install nodejs`

---

## Installer Multichain : 

Ensuite, il faut s'assurer que Multichain est correctement installée sur votre serveur (des problèmes surviennent lorsqu'on l'installe en même temps que le reste).

Pour cela, vous pouvez utiliser le script `install_multichain.sh`

> Placez-vous dans le répertoire à l'intérieur duquel vous souhaitez installer l'API (ex : /home/ubuntu/plaanet)

- Téléchargez le script, donnez lui les droits d'executions, et executez : 

`sudo wget https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/install_multichain.sh`

`sudo chmod 755 install_multichain.sh`

`sudo ./install_multichain.sh`

Normalement, tous les paquets nécessaires au bon fonctionnement de la Multichain seront installés, et le script principal terminera la configuration de la blockchain un peu plus tard.

- Ou bien, executez les commandes du script une à une (si le script ne fonctionne pas) : 

`sudo wget https://www.multichain.com/download/multichain-2.1.2.tar.gz`

`sudo tar -xvzf multichain-2.1.2.tar.gz`

`cd multichain-2.1.2`

`sudo mv multichaind multichain-cli multichain-util /usr/local/bin`

> Passons maintenant au chose sérieuses !

---

## Installation principale : 

> Si ce n'est pas déjà fait : placez-vous dans le répertoire à l'intérieur duquel vous souhaitez installer l'API (ex : /home/ubuntu/plaanet)

Le reste de l'installation sera effectuée automatiquement.

Télécharger le script d'installation, donnez les droits d'execution, et exécutez : 

`sudo wget https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/plaanet-init.sh`

`sudo chmod 755 plaanet-init.sh`

`sudo ./plaanet-init.sh`

Indiquez alors les valeurs des différents paramètres à utiliser, comme expliqué précédemment, et allez vous servir un café bien chaud.

Si tout se déroule correctement, à la fin de l'execution de ce script, votre API sera lancée, et est accessible à tous depuis l'URL que vous aurez défini via les paramètres !

https://$instance_domaine:$instance_port/instance/exists


---

### Lancer/Arrêter l'API :

`sudo systemctl start plaanet-api`

`sudo systemctl stop plaanet-api`

### Vérifier si l'api est active : 

`sudo systemctl status plaanet-api`

### Afficher les logs de l'api en direct : 

`tail -f -n 50 /my/path/to/plaanet/logs/plaanet-api.log`

---

## Est-ce que tout fonctionne ? 🤞
---


Lorsque le service de l'API est lancée, et le reverse-proxy configuré correctement, vous pouvez accéder à votre URL : `https://{instance_domaine}:{instance_port}/instance/exists`

ex : https://plaanet.io:444/instance/exists 

Tant que votre API n'est pas initialisée, cette URL vous donnera les valeurs suivantes :

`{
  error: false,
  instance: false
}`

Si c'est le cas, alors normalement tout est prêt pour initialiser votre instance, et connecter votre serveur au reste du réseau. 🤓

---

## Si l'API ne répond pas... 

Vous pouvez essayer de relancer l'installation en utilisant d'autres paramètres. 

Si le problème persiste : 

- vérifiez l'état global de votre système
- vérifiez que tous les ports sont bien ouverts

Vérifiez que le fichier de configuration de l'API contient bien les valeurs que vous avez demandées : 

`less /my/path/to/plaanet/plaanet-api/config/production.json`

Vérifiez que le reverse-proxy Caddy fonctionne : 

`cd /my/path/to/plaanet` #Se placer où se trouve le fichier Caddyfile

`less Caddyfile` #afficher le contenu du fichier de configuration du proxy

`sudo caddy stop` #arrêter le proxy

`sudo caddy start` #lancer le proxy

Vérifier que la base de donnée MongoDB fonctionne : `mongo`

Si ça marche : `exit`

Si ça marche pas : réinstaller MongoDB à la main.

Mongod doit être lancé avant de lancer l'API.

Si l'API démarre avant Mongo, il faut : 

`sudo systemctl restart plaanet-api.service`

---

## Tout est ok ? 

Lorsque tout est en place et que vous avez une réponse valide de votre serveur, il reste une dernière étape à effectuer afin de CONNECTER votre nouvelle instance au reste du réseau.

Pour cela, vous devez simplement remplir le formulaire qui se trouve à cet adresse : https://plaanet.io/boot-node, afin d'établir un premier contact ! 

Si votre API est correctement configurée, et que le serveur principal (https://plaanet.io) est en mesure de dialoguer avec votre serveur, vous serez automatiquement intégré au reste du réseau.

Les administrateurs de chaque instance recevront une notification afin d'être informée de votre arrivée, et devront activer votre instance pour autoriser la communication entre vos serveurs.

---

# Enregistrement de votre nouvelle instance : 

Pour remplir le formulaire, vous devrez indiquer le InitToken de votre serveur.

Un nouveau token est généré à chaque fois que vous démarrez votre API, jusqu'à ce que votre instance soit correctement initialisée.

Ce token se trouve dans le fichier de logs de votre API : 

`less plaanet/logs/plaanet-api.log`

Vous le trouverez dans les premières lignes du fichier, par exemple : 

`INFO: Init token: 6d3e82de-c46d-4838-982a-9a94dae022b8`

Remplissez correctement le formulaire, indiquez une position géographique en sélectionnant sur la carte, et cliquez sur le bouton "BOOT".

Si tout se passe bien, votre instance sera initialisée et diffusée aux autres instances pour pouvoir dialoguer.

Par défaut, votre instance sera bloquée par les autres instances. 

Par mesure de sécurité, chaque administrateur devra débloquer manuellement (lockIn lockOut) votre instance pour activer la communication entre vos serveurs.

# Accéder à votre instance : 

Une fois le formulaire rempli et validé, si votre serveur est visible dans la liste des serveurs du réseau ici : https://plaanet.io/status, cela signifie que vous pouvez créer votre premier compte utilisateur (sans attendre les autorisations des autres serveurs)

Le premier compte créé est automatiquement un compte avec le rôle isAdmin=true, donc ne tardez pas à créer votre compte.

Vous pourrez alors vous connecter à votre compte, sur votre propre instance, mais vous n'aurez pas accès au contenu des autres serveurs tant qu'ils n'auront pas autorisé votre serveur de leur côté.

En attendant : votre instance est fonctionnelle et peut commencer à accueillir des utilisateurs.

Les administrateurs des autres instances recevront une notification pour les inviter à autoriser votre serveur manuellement.
