
echo "### Installation de la blockchain Multichain"
echo ""
sudo wget https://www.multichain.com/download/multichain-2.1.2.tar.gz
sudo tar -xvzf multichain-2.1.2.tar.gz
cd multichain-2.1.2
sudo mv multichaind multichain-cli multichain-util /usr/local/bin

echo ""
echo "ls /usr/local/bin:"
ls /usr/local/bin

echo ""
echo "> Vérifiez l'existance des repertoires multichaind, multichain-cli, et multichain-util dans /usr/local/bin"
echo "> S'ils existent : Multichain est installée"