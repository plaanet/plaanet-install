# >>> if you already have it : comment it 

echo '>>> IMPORTANT : before to run this script : replace all "johnny" by your own username'
echo '>>> AND modify all path as needed by YOUR system'
echo ''

echo '### install mongo'
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl daemon-reload
sudo systemctl start mongod
sudo systemctl enable mongod

#echo '### is mongo running ?'
#sudo systemctl status mongod

sudo mkdir /data
sudo mkdir /data/db
sudo chown -R johnny:johnny /data

mkdir /home/johnny/logs
mkdir /home/johnny/logs/mongo

#echo '### install git'
#sudo apt-get install -y git-all

echo '### install npm'
sudo apt-get install -y npm
sudo npm i npm@latest -g
