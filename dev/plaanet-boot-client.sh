echo '***********************************'
echo 'LETS TRY TO INSTALL THIS PLAANET CLIENT !'
echo '***********************************'

echo '### install plaanet'
mkdir /home/johnny/Node

echo '### clone plaanet-client'
cd /home/johnny/Node
git clone https://gitlab.com/plaanet/plaanet-client.git
cd plaanet-client

echo '### npm install plaanet-client'
sudo cp ./src/config/default_tmp.json ./src/config/development.json
sudo npm install

echo '***********'
echo 'BOOT DONE !'
echo '***********'
echo ''
echo '> TO START CLIENT, USE FOLLOWING COMMANDE :'
echo 'cd /home/johnny/Node/plaanet-client'
echo 'npm run serve'
echo '>> then CLIENT is running on http://localhost:8080'